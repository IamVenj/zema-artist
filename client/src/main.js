// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
require('../node_modules/bootstrap/dist/css/bootstrap.min.css')
require('./assets/css/light_style.css')
import { library } from '@fortawesome/fontawesome-svg-core'
import { faFacebook } from '@fortawesome/free-brands-svg-icons/faFacebook'
import { faGooglePlusG } from '@fortawesome/free-brands-svg-icons/faGooglePlusG'
import { faKey } from '@fortawesome/free-solid-svg-icons/faKey'
import { faUserCircle } from '@fortawesome/free-solid-svg-icons/faUserCircle'
import { faEye } from '@fortawesome/free-solid-svg-icons/faEye'
import { faEyeSlash } from '@fortawesome/free-solid-svg-icons/faEyeSlash'
import { faSearch } from '@fortawesome/free-solid-svg-icons/faSearch'
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons/faExternalLinkAlt'
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons/faPaperPlane'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Datepicker from 'vuejs-datepicker';

Vue.config.productionTip = false
library.add(faKey, faUserCircle, faExternalLinkAlt, faFacebook, faGooglePlusG, faEye, faEyeSlash, faSearch, faPaperPlane)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('datepicker', Datepicker)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
