import Vue from 'vue'
import Router from 'vue-router'
import WelcomeComponent from '@/components/Welcome/WelcomeComponent'
import SigninComponent from '@/components/auth/SigninComponent'
import SignupComponent from '@/components/auth/SignupComponent'
import ClaimProfileComponent from '@/components/auth/ClaimProfileComponent'
import ForgotPasswordComponent from '@/components/auth/ForgotPasswordComponent'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'WelcomeComponent',
      component: WelcomeComponent
    },
    {
      path: '/login',
      name: 'LoginComponent',
      component: SigninComponent
    },
    {
      path: '/signup',
      name: 'SignupComponent',
      component: SignupComponent
    },
    {
      path: '/claim',
      name: 'ClaimProfileComponent',
      component: ClaimProfileComponent
    },
    {
      path: '/forgot-password',
      name: 'ForgotPasswordComponent',
      component: ForgotPasswordComponent
    }
  ]
})
